#!/usr/bin/env python

from Bio import AlignIO
import numpy as np
import sys
import argparse
import math

parser = argparse.ArgumentParser(description='This script reports the first and last positions in a fasta alignment \
                                              that have greater than 50% non-gap ("-") characters.')

required = parser.add_argument_group('required arguments')

required.add_argument("input_alignment", metavar='fasta_file', type=str, nargs="+", help="Input alignment file.")

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(0)

args = parser.parse_args()

alignment = AlignIO.read(args.input_alignment[0],"fasta")

align_array = np.array([list(rec) for rec in alignment], np.character)

# some of the GISAID alignments have "n" characters at the start preceding gap characters ("-"), which was messing things (early positions could have more than have "n")
# this converts all "n" characters to gaps to let us still get the start and stop positions with greater than 50% non-gap and non-n characters
align_array = np.core.defchararray.replace(align_array, b"n", b"-")

num_seqs = align_array.shape[0]

num_of_seqs_greater_than_50_percent = math.ceil(num_seqs / 2)

gaps_per_column = np.count_nonzero(align_array == b"-", axis = 0)

non_gaps_per_column = num_seqs - gaps_per_column

# first position index (1-based) that have above 50% non-gap characters
start_one_based = (non_gaps_per_column >= num_of_seqs_greater_than_50_percent).nonzero()[0][0] + 1

# last position index (1-based) that has above 50% non-gap characters
end_one_based = (non_gaps_per_column >= num_of_seqs_greater_than_50_percent).nonzero()[0][-1] + 1

print("First position with >50% non-gap characters: " + str(start_one_based))
print("Last position: " + str(end_one_based))