### This directory contains the work for performing the full alignment of GISAID assemblies, calculating the Shannon uncertainty for each column, and generating the figure
### Contact Michael D. Lee for any questions or inquiries: Mike.Lee@nasa.gov


## conda environment can be created with the same versions as so

conda create -n Lee-assembly-variation -c conda-forge -c bioconda -c defaults -c astrobiomike mafft=7.458 bit=1.8.02
conda activate Lee-assembly-variation

# alignment with mafft
mafft --auto all-Sapoval-et-al-GISAID-and-ref.fa > all-Sapoval-et-al-GISAID-and-ref-mafft-align.fa

# calculating variation 
bit-calc-variation-in-msa -i all-Sapoval-et-al-GISAID-and-ref-mafft-align.fa -t DNA -o all-Sapoval-et-al-GISAID-and-ref-mafft-align-variation.tsv

# getting start and stop positions where greater than 50% of the bases in the alignment are non-gap characters (filters out noise at start and end)
python ad-hoc-get-start-and-stop-pos.py all-Sapoval-et-al-GISAID-and-ref-mafft-align.fa


# getting map of ref coordinates to alignment coordinates
head -n 2 all-Sapoval-et-al-GISAID-and-ref.fa > ref-seq.fa
python ad-hoc-map-ref-positions-to-alignment.py ref-seq.fa

# rest of work done in R script "plotting-variation-and-overlaying-features.R", and then beautified in affinity designer