import random
import csv
import sys
import pickle
import os

"""
This script uses the metadata file for the sequence to map countries
to different colors, and it thereafter adds this information to an
iTOL annotation file.

INPUT: metadatafile path and annotation file path.
Usage: python continentcolor.py gisaid_metadata.csv cont_anno_temp.txt

OUTPUT: adds csv info to input annotation file.
example: GISAID_97.fasta,#2E244E,COL##2E294E
"""

# mini usage menu if no args passed
if (len(sys.argv) == 1) or \
   (sys.argv[1] == '-h') or \
   (sys.argv[1] == '--help'):
    print( """
           USAGE:
           python continentcolor.py gisaid_metadata.csv cont_anno_temp.txt
           """
         )
    exit()

# opening files.
script = os.path.dirname(__file__) + "/"
metasata_csv_csvreader = csv.reader(open(sys.argv[1]), delimiter=',')
color_file = open(sys.argv[2], "a+")
chinese_cities_dict = pickle.load( open( script + "chinese_cities_dict.p", "rb" ) )

# country to continent map
country_to_continent = {   "Abkhazia" : "Asia",
                           "Afghanistan" : "Asia",
                           "Fujian" : "Asia",
                           "Jian" : "Asia",
                           "Zhejiang" : "Asia",
                           "Nonthaburi" : "Asia",
                           "Albania" : "Europe",
                           "Jiangsu" : "Asia",
                           "Wuhan-Hu-1" : "Asia",
                           "Yunnan" : "Asia",
                           "Shandong" : "Asia",
                           "Korea" : "Asia",
                           "Sichuan" : "Asia",
                           "Jiangxi" : "Asia",
                           "Algeria" : "Africa",
                           "American Samoa" : "Oceania",
                           "Andorra" : "Europe",
                           "Angola" : "Africa",
                           "Anguilla" : "North America",
                           "Antigua and Barbuda" : "North America",
                           "Argentina" : "South America",
                           "Armenia" : "Asia",
                           "Aruba" : "North America",
                           "Australia" : "Oceania",
                           "Austria" : "Europe",
                           "Azerbaijan" : "Asia",
                           "Bahamas" : "North America",
                           "Bahrain" : "Asia",
                           "Bangladesh" : "Asia",
                           "Barbados" : "North America",
                           "Belarus" : "Europe",
                           "Belgium" : "Europe",
                           "Belize" : "North America",
                           "Benin" : "Africa",
                           "Bermuda" : "North America",
                           "Bhutan" : "Asia",
                           "Bolivia" : "South America",
                           "Bonaire" : "North America",
                           "Bosnia and Herzegovina" : "Europe",
                           "Botswana" : "Africa",
                           "Bouvet Island" : "Antarctica",
                           "Brazil" : "South America",
                           "British Indian Ocean Territory" : "Asia",
                           "British Virgin Islands" : "North America",
                           "Virgin Islands, British" : "North America",
                           "Brunei" : "Asia",
                           "Brunei Darussalam" : "Asia",
                           "Bulgaria" : "Europe",
                           "Burkina Faso" : "Africa",
                           "Burundi" : "Africa",
                           "Cambodia" : "Asia",
                           "Cameroon" : "Africa",
                           "Canada" : "North America",
                           "Cape Verde" : "Africa",
                           "Cayman Islands" : "North America",
                           "Central African Republic" : "Africa",
                           "Chad" : "Africa",
                           "Chile" : "South America",
                           "China" : "Asia",
                           "Christmas Island" : "Asia",
                           "Cocos (Keeling) Islands" : "Asia",
                           "Colombia" : "South America",
                           "Comoros" : "Africa",
                           "Congo" : "Africa",
                           "Congo, Republic of" : "Africa",
                           "Republic of the Congo" : "Africa",
                           "DRC" : "Africa",
                           "Cook Islands" : "Oceania",
                           "Costa Rica" : "North America",
                           "Croatia" : "Europe",
                           "Cuba" : "North America",
                           "Curaçao" : "North America",
                           "Cyprus" : "Asia",
                           "Czech Republic" : "Europe",
                           "Congo, Democratic Republic of" : "Africa",
                           "Democratic Republic of the Congo" : "Africa",
                           "Denmark" : "Europe",
                           "Djibouti" : "Africa",
                           "Dominica" : "North America",
                           "Dominican Republic" : "North America",
                           "East Timor" : "Asia",
                           "Ecuador" : "South America",
                           "England" : "Europe",
                           "Egypt" : "Africa",
                           "El Salvador" : "North America",
                           "Equatorial Guinea" : "Africa",
                           "Eritrea" : "Africa",
                           "Estonia" : "Europe",
                           "Ethiopia" : "Africa",
                           "Falkland Islands" : "South America",
                           "Faroe Islands" : "Europe",
                           "Fiji" : "Oceania",
                           "Finland" : "Europe",
                           "France" : "Europe",
                           "French Guiana" : "South America",
                           "French Polynesia" : "Oceania",
                           "Gabon" : "Africa",
                           "Gambia" : "Africa",
                           "Guangdong" : "Asia",
                           "Georgia" : "Asia",
                           "Germany" : "Europe",
                           "Ghana" : "Africa",
                           "Gibraltar" : "Europe",
                           "Greece" : "Europe",
                           "Greenland" : "North America",
                           "Grenada" : "North America",
                           "Guadeloupe" : "North America",
                           "Great Britain" : "Europe",
                           "Guam" : "Oceania",
                           "Guatemala" : "North America",
                           "Guernsey" : "Europe",
                           "Guinea" : "Africa",
                           "Guinea-Bissau" : "Africa",
                           "Guyana" : "South America",
                           "Haiti" : "North America",
                           "Heard Island and McDonald Islands" : "Antarctica",
                           "Honduras" : "North America",
                           "Hong Kong" : "Asia",
                           "Hungary" : "Europe",
                           "Iceland" : "Europe",
                           "India" : "Asia",
                           "Indonesia" : "Asia",
                           "Iran" : "Asia",
                           "Iraq" : "Asia",
                           "Ireland" : "Europe",
                           "Isle of Man" : "Europe",
                           "Islamic Republic of Iran" : "Asia",
                           "Israel" : "Asia",
                           "Italy" : "Europe",
                           "Ivory Coast" : "Africa",
                           "Jamaica" : "North America",
                           "Japan" : "Asia",
                           "Jersey" : "Europe",
                           "Jordan" : "Asia",
                           "Kazakhstan" : "Asia",
                           "Kenya" : "Africa",
                           "Korea, Democratic People's Republic of" : "Asia",
                           "Kiribati" : "Oceania",
                           "Korea, Republic Of" : "Asia",
                           "Kosovo" : "Europe",
                           "Kuwait" : "Asia",
                           "Kyrgyzstan" : "Asia",
                           "Laos" : "Asia",
                           "Lao People's Democratic Republic" : "Asia",
                           "Latvia" : "Europe",
                           "Lebanon" : "Asia",
                           "Lesotho" : "Africa",
                           "Liberia" : "Africa",
                           "Libya" : "Africa",
                           "Liechtenstein" : "Europe",
                           "Lithuania" : "Europe",
                           "Luxembourg" : "Europe",
                           "Macau" : "Asia",
                           "Macedonia" : "Europe",
                           "Macedonia, The Former Yugoslav Republic Of" : "Europe",
                           "Madagascar" : "Africa",
                           "Malawi" : "Africa",
                           "Malaysia" : "Asia",
                           "Maldives" : "Asia",
                           "Mali" : "Africa",
                           "Malta" : "Europe",
                           "Marshall Islands" : "Oceania",
                           "Martinique" : "North America",
                           "Mauritania" : "Africa",
                           "Mauritius" : "Africa",
                           "Mayotte" : "Africa",
                           "Mexico" : "North America",
                           "Micronesia" : "Oceania",
                           "Micronesia, Federated States of" : "Oceania",
                           "Moldova" : "Europe",
                           "Moldova, Republic Of" : "Europe",
                           "Monaco" : "Europe",
                           "Mongolia" : "Asia",
                           "Montenegro" : "Europe",
                           "Montserrat" : "North America",
                           "Morocco" : "Africa",
                           "Mozambique" : "Africa",
                           "Myanmar" : "Asia",
                           "NanChang" : "Asia",
                           "Namibia" : "Africa",
                           "Nauru" : "Oceania",
                           "Nepal" : "Asia",
                           "Netherlands" : "Europe",
                           "New Caledonia" : "Oceania",
                           "New Zealand" : "Oceania",
                           "Nicaragua" : "North America",
                           "Niger" : "Africa",
                           "Nigeria" : "Africa",
                           "Niue" : "Oceania",
                           "Norfolk Island" : "Oceania",
                           "North Korea" : "Asia",
                           "Northern Cyprus" : "Asia",
                           "Northern Mariana Islands" : "Oceania",
                           "Norway" : "Europe",
                           "Oman" : "Asia",
                           "Pakistan" : "Asia",
                           "Palau" : "Oceania",
                           "Palestine" : "Asia",
                           "Panama" : "North America",
                           "Papua New Guinea" : "Oceania",
                           "Paraguay" : "South America",
                           "Peru" : "South America",
                           "Philippines" : "Asia",
                           "Poland" : "Europe",
                           "Portugal" : "Europe",
                           "Puerto Rico" : "North America",
                           "Qatar" : "Asia",
                           "Romania" : "Europe",
                           "Russia" : "Europe",
                           "Russian Federation" : "Europe",
                           "Rwanda" : "Africa",
                           "Réunion" : "Africa",
                           "Saba" : "North America",
                           "Scotland" : "Europe",
                           "Saint Barthélemy" : "North America",
                           "Saint Helena, Ascension and Tristan da Cunha" : "Africa",
                           "Saint Kitts and Nevis" : "North America",
                           "St. Kitts and Nevis" : "North America",
                           "Saint Lucia" : "North America",
                           "St. Lucia" : "North America",
                           "Saint Martin" : "North America",
                           "St. Martin" : "North America",
                           "Saint Pierre and Miquelon" : "North America",
                           "St. Pierre and Miquelon" : "North America",
                           "Saint Vincent and the Grenadines" : "North America",
                           "St. Vincent and The Grenadines" : "North America",
                           "Samoa" : "Oceania",
                           "San Marino" : "Europe",
                           "Saudi Arabia" : "Asia",
                           "Senegal" : "Africa",
                           "Serbia" : "Europe",
                           "Seychelles" : "Africa",
                           "Sierra Leone" : "Africa",
                           "Singapore" : "Asia",
                           "Sint Eustatius" : "North America",
                           "Slovakia" : "Europe",
                           "Slovenia" : "Europe",
                           "Solomon Islands" : "Oceania",
                           "Somalia" : "Africa",
                           "Somaliland" : "Africa",
                           "South Africa" : "Africa",
                           "South Georgia and the South Sandwich Islands" : "South America",
                           "South Korea" : "Asia",
                           "South Ossetia" : "Asia",
                           "South Sudan" : "Africa",
                           "Spain" : "Europe",
                           "Sri Lanka" : "Asia",
                           "Sudan" : "Africa",
                           "Suriname" : "South America",
                           "Svalbard" : "Europe",
                           "Swaziland" : "Africa",
                           "Sweden" : "Europe",
                           "Switzerland" : "Europe",
                           "Syria" : "Asia",
                           "Syrian Arab Republic" : "Asia",
                           "São Tomé and Príncipe" : "Africa",
                           "Taiwan" : "Asia",
                           "Taiwan, Province of China" : "Asia",
                           "Tajikistan" : "Asia",
                           "Tanzania" : "Africa",
                           "Tanzania, United Republic Of" : "Africa",
                           "Thailand" : "Asia",
                           "Togo" : "Africa",
                           "Tokelau" : "Oceania",
                           "Tonga" : "Oceania",
                           "Trinidad and Tobago" : "North America",
                           "Tunisia" : "Africa",
                           "Turkey" : "Asia",
                           "Turkmenistan" : "Asia",
                           "Turks and Caicos Islands" : "North America",
                           "Turks and Caicos" : "North America",
                           "Tuvalu" : "Oceania",
                           "Uganda" : "Africa",
                           "Ukraine" : "Europe",
                           "United Arab Emirates" : "Asia",
                           "United Kingdom" : "Europe",
                           "United States Virgin Islands" : "North America",
                           "United States" : "North America",
                           "United States of America" : "North America",
                           "USA" : "North America",
                           "Uruguay" : "South America",
                           "Uzbekistan" : "Asia",
                           "Vanuatu" : "Oceania",
                           "Venezuela" : "South America",
                           "Vietnam" : "Asia",
                           "Wallis and Futuna" : "Oceania",
                           "Wales" : "Europe",
                           "Yemen" : "Asia",
                           "Zambia" : "Africa",
                           "Zimbabwe" : "Africa",
                           "Åland Islands" : "Europe"}

CONT_COLORS = ['#EA3546','#454ADE','#000000','#ACF39D','#FFFFFF','#9D44B5']
               #"#FFBE0B", "#FB5607", "#FF006E", "#8338EC", "#3A86FF", "#E63B2E"]

# find all countries and continents
country_count = {}
continent_count = {}
fasta_to_country_dict = {}
fasta_to_continent_dict = {}
countries = set()
continents = set()
number_of_genomes = 0
for row in metasata_csv_csvreader:
    country = row[2] #grab country from csv
    if country in chinese_cities_dict.keys():
        country = "China"

    # countries
    if country in country_count:
        country_count[country] += 1
    else:
        country_count[country] = 1
    fasta_to_country_dict[row[0]] = country
    countries.add(country)

    # continentcountry
    continent_temp = country_to_continent[country]
    continents.add(continent_temp)
    if continent_temp in continent_count:
        continent_count[continent_temp] += 1
    else:
        continent_count[continent_temp] = 1
    fasta_to_continent_dict[row[0]] = continent_temp

    #count # genomes
    number_of_genomes += 1

# assign all countries to a color.
country_to_hex = {}
for country_name in countries:
    rand_numb = random.randint(0,16777215)
    temp_hex = '#' + str(hex(rand_numb))[2:]
    country_to_hex[country_name] = temp_hex


continent_to_hex = {}
cont_count = 0
for continent_name in continents:
    #rand_numb = random.randint(0,16777215)
    #temp_hex = '#' + str(hex(rand_numb))[2:]
    temp_hex = CONT_COLORS[cont_count]
    continent_to_hex[continent_name] = temp_hex
    cont_count += 1

# make legend.

## new legend (by continent)
legend_title = "Continent by Color"

value_string = ','.join([ continent_to_hex[continent_t]
                          for continent_t in continents ])
country_string = ','.join([ str(continent_t) + " (" +
                            str( round(continent_count[continent_t] /
                                 number_of_genomes * 100,2)) + "%)"
                                 for continent_t in continents ])
shape_string = ','.join([ "1" for continent_t in continents ])

## old legend (by country)
#legendcuttoff = 10
#legend_title = "Country by Color"
#topcountries = [k for k, v in sorted(country_count.items(), key=lambda item:
#    item[1])][::-1][:legendcuttoff]
#country_string = ','.join([ str(country_t) + " (" +
#    str( round(country_count[country_t] / number_of_genomes * 100,2)) + "%)" for country_t in topcountries ])
#value_string = ','.join([ country_to_hex[country_t] for country_t in topcountries ])
#shape_string = ','.join([ "1" for country_t in topcountries ])

# write legend information to file. 
color_file.write("\n")
color_file.write(f"LEGEND_TITLE,{legend_title}")
color_file.write("\n")
color_file.write(f"LEGEND_SHAPES,{shape_string}")
color_file.write("\n")
color_file.write(f"LEGEND_LABELS,{country_string}")
color_file.write("\n")
color_file.write(f"LEGEND_COLORS,{value_string}")

# map country to color and print with fasta name.
color_file.write("\n")
color_file.write("DATA")
for fasta_name, continent_name in fasta_to_continent_dict.items():
    randhex = continent_to_hex[continent_name]
    color_file.write("\n")
    color_file.write(f"{fasta_name},{randhex},COL#{randhex}")
