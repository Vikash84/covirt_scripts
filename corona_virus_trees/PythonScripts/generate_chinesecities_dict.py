from china_cities import *
import pickle

"""
This script is used to to create the pickled dictionary
of cities in China.

Make sure to download the china-cities  package. 
URL: https://pypi.org/project/china-cities/
Steps: 
      1. git clone https://github.com/boeboe/china-cities.git
      2. python setup.py install
      3. make tests
      4. Run this script. 
         (python generate_chinesecities_dict.py)
"""

chinese_cities_dict = {}
for city in cities.get_cities():
    print("english name:", city.name_en)
    print("chinese name:", city.name_cn)
    print("province:    ", city.province)
    chinese_cities_dict[str(city.name_en)] = 1

pickle.dump( chinese_cities_dict, open( "chinese_cities_dict.p", "wb" ) )
