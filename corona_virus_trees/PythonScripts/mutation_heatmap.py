import sys


"""
This file takes in both a VCF file and a template for the heatmap iTOL
annotation file and add data based on which genomes contain SNPs and SNVs.

INPUT:
    The input for this script is both a parsnp VCF file and a heatmap annotation
    file which can have data appended (such as mutation_heatmap_FILE.txt). Note,
    make sure to make a backup copy of the template file.

OUTPUT:
    The input heatmap annotation file is modified and metadata is added.

Usage:
    python PythonScripts/mutation_heatmap.py \
           Corona_trees-metadata/parsnp.eff.vcf \
           itol_metadata_files/itol_template_files/mutation_heatmap_FILE.txt

"""


# input files.
topnmutations = 13
heatmapfile = open(sys.argv[2], 'a+')

# grab top mutations (note, this grabs top positions, not exact mutations)
mutational_array = {}
mutational_count = {}
for line in open(sys.argv[1]).readlines():
    # set up the dictionary
    if '#CHROM' in line:
        fasta_names =  [bool_array.rstrip("\n") for bool_array in line.split("\t")[10:]]
    if '#' not in line:
        line = line.split("\t")
        position, ref_temp, mut_temp = line[1], line[3], line[4]
        if (ref_temp != 'N') and (mut_temp != 'N'):
            genomes_with_boolarray = [int(bool_val) for bool_val in line[9:]]
            mutcount_int = sum(genomes_with_boolarray)
            if position in mutational_count:
                mutational_count[position] += int(mutcount_int)
            else:
                mutational_count[position] = int(mutcount_int)
            if position in mutational_array:
                mutational_array[position] += genomes_with_boolarray
            else:
                mutational_array[position] = genomes_with_boolarray

mutational_count = {k: v for k, v in sorted(mutational_count.items(), key=lambda item: item[1])}
top_mutations_array = list(mutational_count.keys())[::-1][:topnmutations]
print(top_mutations_array)

# make output dict { fastaname : [0,1,1,0,...,0]}
mutpresence_dict = {}
for topmut in top_mutations_array:
    topmutarray = mutational_array[topmut]
    for genome_index in range(len(fasta_names)):
        genome = fasta_names[genome_index]
        bool_val = topmutarray[genome_index]
        if genome not in mutpresence_dict:
            mutpresence_dict[genome] = [bool_val]
        else:
            mutpresence_dict[genome].append(bool_val)

## writing to file
mut_name_string=','.join([str(mut) for mut in top_mutations_array])
mut_name_string2=f"FIELD_LABELS,{mut_name_string}"
heatmapfile.write("\n")
heatmapfile.write(mut_name_string2)
heatmapfile.write("\n")
heatmapfile.write("DATA")

for fastaname in fasta_names:
    mutarray = mutpresence_dict[fastaname]
    mutstring = ",".join([str(mut_bool) for mut_bool in mutarray])
    fullsampstring = f"{fastaname},{mutstring}"
    heatmapfile.write("\n")
    heatmapfile.write(fullsampstring)

