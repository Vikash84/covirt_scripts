### Contribute

Each subsection of the "Materials and Methods" section should have a branch in the repository.
Each branch should have a top level folder w/ the subsection from Mat&methods it describes. Inside that folder is where the scripts and documentation goes. The README should document
* Where the data resides on SNO/GHO
* How the data was obtained
* Commands used to analyze that data to get the results in the manuscript.
* Third party tools/python modules required
